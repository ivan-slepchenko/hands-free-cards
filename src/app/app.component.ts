import { Component, ChangeDetectorRef } from '@angular/core';

interface Card {
    answers: string;
	question: string[];
	points: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
	constructor(private cdr: ChangeDetectorRef) { }

	allWords = [];
	wordsPool = [];
	
	index = -1;
	sequence = 600;
	volume = 1;
	recognitionDelay = 500;
	recognizedWord = "";

	answerDetected = false;
	answerRecognition = null;
	delayed = false;

	remember() {
		this.previousAnswers.push(this.recognizedWord);
		this.commit();
	}

	ngOnInit() { 
		let db = localStorage.getItem('db');
		if (db === null) {
			this.reset();
			db = localStorage.getItem('db');
		}
		this.allWords = JSON.parse(db).words;
		this.rearrangeWords();
	}

	reset() {
		this.allWords = [
			{ "answers": ["je suis pressé", "je suis précis"], "question": "я спешу" },
			{ "answers": ["mon savoir"], "question": "мое знание" },
			{ "answers": ["je veux", "j'avoue"], "question": "я хочу" },
			{ "answers": ["je ne comprends pas"], "question": "я не понимаю" },
			{ "answers": ["je vais lui demander"], "question": "я спрошу его" },
			{ "answers": ["je veux te trouver"], "question": "я хочу тебя найти" },
			{ "answers": ["rends le moi"], "question": "верни его мне" },
			{ "answers": ["laisse la tenir"], "question": "дай ей подержать" },
			{ "answers": ["prends-le s'il te plaît"], "question": "отнеси это пожалуйста" },
			{ "answers": ["montrez-moi les documents"], "question": "покажи мне документы" },
			{ "answers": ["bienvenue chez nous"], "question": "добро пожаловать к нам" },
			{ "answers": ["à tout à l’heure"], "question": "до скорого!" },
			{ "answers": ["pourriez-vous m’aider?"], "question": "не могли бы вы мне помочь?" },
			{ "answers": ["où sont des toilettes?"], "question": "where are the toilets?" },
			{ "answers": ["où sommes-nous?"], "question": "где мы?" },
			{ "answers": ["à quelle heure faut-il arriver?"], "question": "в котором часу я должен прибыть?" },
			{ "answers": ["je voudrais une bière."], "question": "я хотел бы одно пиво." },
			{ "answers": ["l’addition, s'il vous plaît"], "question": "счет пожалуйста." },
			{ "answers": ["merci beaucoup"], "question": "спасибо вам большое" },
			{ "answers": ["comment dire ça en français?"], "question": "как сказать это по-французски?" },
		];

		this.allWords.forEach((untyped:any) => {
			let card: Card = untyped;
			card.points = 0;
		});
	
		this.commit();
	}

	rearrangeWords() {
		let sortedWords = this.allWords.slice(0).sort((a, b) => {
			return a.points - b.points;
		});
		for (let i = 0; i < sortedWords.length; i++) {
			if (this.wordsPool.length < 5) {
				this.wordsPool.push(sortedWords[i]);
			} else {
				return;
			}
		}
	} 

	commit() {
		localStorage.setItem('db', JSON.stringify({
			words: this.allWords
		}));
	}

	pointUp() {
		this.wordsPool[this.index].points = this.wordsPool[this.index].points + 1;
		this.commit();

		if (this.wordsPool[this.index].points === 5) {
			this.wordsPool.splice(this.index, 1);
		}
	}

	pointDown() {
		this.wordsPool[this.index].points = this.wordsPool[this.index].points - 1;
		this.commit();
	}

	get points() {
		return this.index === -1 ? ':)' : this.wordsPool[this.index].points;
	}

	get question() {
		return this.index === -1 ? `ПОГНАЛИ` : this.wordsPool[this.index].question;
	}

	get previousAnswers() {
		let previousIndex = this.index - 1;
		if (previousIndex === -1) previousIndex = this.answers.length - 1; 
		return this.wordsPool[previousIndex].answers;
	}

	get previousAnswer() {
		return this.previousAnswers[0];
	}

	get answer() {
		return this.index==-1? 'ОТВЕТ' : this.answers[0];
	}

	get answers() {
		return this.wordsPool[this.index].answers;
	}

	next()
	{
		this.rearrangeWords();
		this.index = this.index + 1;
		if (this.index === this.wordsPool.length)
		{
			this.index = 0;	
		}

		this.ask();
		this.cdr.detectChanges();
	}

	handleAnswerRecognized(event) {
		var result = event.results[0];
		if (result.isFinal) {
			this.answerRecognition.onend = () => { };
			this.answerRecognition.abort();
			let word = result[0].transcript.toLowerCase();
			console.log(`WORD: ${word}`);
			if (this.delayed)
			{
				if (word === "continue") {
					this.delayed = false;
					var speech = new SpeechSynthesisUtterance("Ok, continuons.");
					speech.lang = 'fr';
					speech.volume = 1;
					speechSynthesis.speak(speech);
					speech.onend = () => { this.ask() };
					this.answerDetected = true;
				} else {
					var speech = new SpeechSynthesisUtterance("Je ne comprend pas!");
					speech.lang = 'fr';
					speech.volume = 1;
					speechSynthesis.speak(speech);
					speech.onend = () => { this.recognizeAnswer() };
				}
			}
			else if (word === "stop") {		
				this.delayed = true;
				var speech = new SpeechSynthesisUtterance("Je comprends, j'attendrai!");
				speech.lang = 'fr';
				speech.volume = 1;
				speechSynthesis.speak(speech);
				this.answerDetected = true;
				speech.onend = () => {
					this.answerDetected = false;
					this.recognizeAnswer()
				};
			} else {
				const isRight = this.answers.includes(word.toLowerCase());
				this.recognizedWord = word; 
				isRight ? this.pointUp() : this.pointDown();
				var speech = new SpeechSynthesisUtterance(isRight ? `Correctement!` : `Faux! ${this.answers[0]}!`);
				speech.lang = 'fr';
				speech.volume = 1;
				speech.rate = isRight ? 1 : 0.75;
				speechSynthesis.speak(speech);
				speech.onend = () => { this.next() };
				this.answerDetected = true;
			}
		}
	}

	handleAnswerRecognitionEnd(event) {
		if (!this.answerDetected) {
			this.recognizeAnswerImmediatelly();
		}
	}


	ask() {
		this.answerDetected = false;
		const speechSynthesis = window['speechSynthesis'];
		const speech = new SpeechSynthesisUtterance(this.question);
		speech.lang = 'ru';
		speech.volume = 1;
		speech.onend = () => {
			if (this.points < 1) {
				const speechSynthesis2 = window['speechSynthesis'];
				const speech2 = new SpeechSynthesisUtterance(this.answers[0]);
				speech2.lang = 'fr';
				speech2.volume = 1;
				speech2.onend = () => {
					this.recognizeAnswer();
				}
				speechSynthesis2.speak(speech2);
			} else {
				this.recognizeAnswer();
			}
		}
		speechSynthesis.speak(speech);
	}
	
	recognizeAnswer() {
		setTimeout(() => { this.recognizeAnswerImmediatelly() }, 1000);
	}
	
	recognizeAnswerImmediatelly() {
		const speechRecognition = window['webkitSpeechRecognition'];
		this.answerRecognition = new speechRecognition();
		this.answerRecognition.lang = 'fr';
		this.answerRecognition.continuous = false;
		this.answerRecognition.interimResults = true;
		// this.answerRecognition.maxAlternatives = 2;
		this.answerRecognition.onend = (event) => { this.handleAnswerRecognitionEnd(event) }
		this.answerRecognition.onresult = (event) => { this.handleAnswerRecognized(event) }
		this.answerRecognition.onstart = () => { 
			console.log("IF DELAYED");
			if (!this.delayed) {
				console.log("GO");
				var context = new AudioContext()
				var gainNode = context.createGain()
				gainNode.gain.value = this.volume;
				gainNode.connect(context.destination)
				var o = context.createOscillator()
				o.type = "sine"
				o.frequency.value = this.sequence;
				o.connect(gainNode)	
				o.start()
				setTimeout(() => { o.stop()}, this.recognitionDelay)
			}
		}
		this.answerRecognition.start();
	}
}
 